import axios from 'axios'
import { ErrorAlert } from './alert'

const getHeader = () => {
    const defaultHeader = { 'Authorization': '' };
    if (typeof window === 'undefined') { return defaultHeader }
    if (!localStorage.getItem('user')) { return defaultHeader }
    const user = JSON.parse(localStorage.getItem('user') ?? '');
    const token = `Token ${user.api_key}:${user.api_secret}`
    return { 'Authorization': token }
}

export const httpHandler = axios.create({
    baseURL: 'https://dev.dipmarts.com',
    headers: getHeader(),
})
httpHandler.interceptors.response.use(
    (response) => response,
    (error) => {
        if (error.response.status) {
            ErrorAlert(error.message, true)
        }
    }
)