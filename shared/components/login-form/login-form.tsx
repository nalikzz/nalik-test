import { useForm } from "react-hook-form";

interface LoginForm {
    close: () => void
}

const LogInForm = (props: LoginForm) => {

    const { register, handleSubmit,formState: { errors } } = useForm();

    const submit = (data:any) => {
        console.log('submit',data);
        console.log(errors);
    }

    return (
        <form onSubmit={handleSubmit(submit)} className="flex flex-col gap-2">
            <div className={`${errors.username?'invalid':''} form-input`}>
                <label htmlFor="username">Username</label>
                <input type="text" {...register('username',{required:true})}  ></input>
            </div>
            <div className="form-input">
                <label htmlFor="password">Password</label>
                <input type="password" name="password"></input>
            </div>
            <div className="flex gap-2 mt-2 justify-end">
                <button onClick={props.close} type="button" className='border rounded-md shadow-lg p-2'>Cancel</button>
                <button type="submit" className='border rounded-md shadow-lg p-2 bg-primary text-white'>Log In</button>
            </div>
        </form>
    );
}

export default LogInForm;