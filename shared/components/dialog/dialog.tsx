import { useEffect, useRef } from 'react';
import LogInForm from '../login-form/login-form';
import style from './dialog.module.css'
const Dialog = (props: { isOpenDialog: boolean, toggleDialog: () => void }) => {

    const dialog:any = useRef(null)
    
    useEffect(() => {
        props.isOpenDialog ? dialog.current.showModal() : dialog.current.close();
        return () => dialog.current.close()
    }, [props.isOpenDialog])


    const handleClickOutside = () => {
        props.toggleDialog();
    };

    const handleClickInside = (e:any)=>{
        e.stopPropagation()
    }

    return (
        <dialog className={`${style.containDialog} `} id='modal' onClick={handleClickOutside} ref={dialog}>
            <div className='rounded-md z-10 p-6' onClick={(e)=>handleClickInside(e)}>
                <h1 className="text-2xl font-bold mb-2">
                    Sign In
                </h1>
                <LogInForm close={props.toggleDialog}></LogInForm>
            </div>
        </dialog>
    );
}


export default Dialog;