import { useState } from 'react'
import Dialog from '../dialog/dialog'
import styles from '../header/header.module.css'

const Header = () => {

    const [isOpenDialog, setOpenDialog] = useState(false)

    const toggleDialog = () => {
        setOpenDialog(!isOpenDialog)
    }

    return (
        <div >
            <button className="border shadow-md rounded-sm p-2" onClick={toggleDialog}>open Dialog</button>
            <Dialog isOpenDialog={isOpenDialog} toggleDialog={toggleDialog}></Dialog>
        </div>
    )
}

export default Header
