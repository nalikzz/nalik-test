export enum ApiEndpointsEnum {
    LOG_IN = '/api/method/dipmarts_app.api.login',
    GUEST_USER = '/api/method/dipmarts_app.api.generate_guest',
    CATEGORY = '/api/method/dipmarts_app.api.categorylist',
    BRAND = '/api/method/dipmarts_app.api.brandlist/',
    HOME_PAGE_PAGINATION = '/api/method/dipmarts_app.api.homepagepg',
    PRODUCT_BY_BRAND = '/api/method/dipmarts_app.api.brandproduct'
}