import type { NextPage } from 'next'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import Header from '../shared/components/header/header'
import { ApiEndpointsEnum } from '../shared/enum/api-end-point.enum'
import { httpHandler } from '../shared/http-interceptor/http-interceptor'

const Home: NextPage = () => {

  const [numbers, setNumbers] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9])
  const [isLoad, setIsLoad] = useState(false)
  const listItems = numbers.map((number, i) =>
    <div className="bg-blue-500 w-full h-40" key={i}>{number} {numbers.length}
      <Link href={'product-listing'}>Hello</Link>
    </div>
  );

  const handleScroll = (e: any) => {
    const scrollHeight = e.target.documentElement.scrollHeight;
    const windowHeight = window.innerHeight + e.target.documentElement.scrollTop + 1


    console.log('home');
    if (windowHeight >= scrollHeight) {
      setNumbers((old) => [...old, ...numbers])
    }
  }

  let run = false;


  useEffect(() => {
    if (!run) {
      run = true;
      window.addEventListener('scroll', handleScroll)
      // return () => window.removeEventListener('scroll', handleScroll);
    }
  })

  return (
    <div>
      {/* <Header></Header> */}
      <div className="flex gap-4 flex-col">
        {listItems}
      </div>
    </div>
  )
}

export default Home


  // useEffect(() => {
  //   return (() => { getGuestUser(); })
  // }, [])

  // const getGuestUser = async () => {
  //   const res = await (await httpHandler.get(ApiEndpointsEnum.GUEST_USER))?.data
  //   localStorage.setItem('user', JSON.stringify(res.message))
  // }