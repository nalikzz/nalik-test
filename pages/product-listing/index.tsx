const ProductListing = () => {

    const numbers = [1, 2, 3, 4, 5,1, 2, 3, 4, 5];
    const listItems = numbers.map((number,i) =>
        <div className="bg-yellow-500 w-full h-40" key={i}>{number}</div>
    );

    return (
        <div className="flex gap-4 flex-col">
            {listItems}
        </div>
    );
}

export default ProductListing;